import React from 'react'
import { Route, Routes } from 'react-router-dom'
// layouts
import DefaultLayout from '../layouts/DefaultLayout'
import HomePage from '../pages/home'
// pages
import LoginPage from '../pages/login'
import RegisterPage from '../pages/register'
// path for app
import { path } from './path'

export default function Router() {
  // get from store
  return (
    <Routes>
      <Route path="/" element={<DefaultLayout />} >
        <Route path={path.home} element={<HomePage />} />
        <Route path={path.login} element={<LoginPage />} />
        <Route path={path.register} element={<RegisterPage />} />
      </Route>
    </Routes>
  )
}

// Không được đứng trực tiếp ở main để thao tác git add or commit or push

// *** muốn biết đang đứng ở branch nào : gõ lệnh : git branch (nhấn chữ q để end git branch)

// 0. create file .gitignore và paste git ignore trên gitlab vào

// 1. Kiểm tra tất cả code đã được save
// 2. Sử dụng lệnh : git checkout -b branch_name ( vd: git checkout -b feat/init-project )
// 3. Sử dụng lệnh : git add . ( để add tất cả các file lên trạng thái staged )
// 4. Sử dụng lệnh : git commit -m 'commit_name' ( vd: git commit -m 'Init project, install libraries support, set up router' )
// 5. Sử dụng lệnh : git push --set-upstream origin branch_name ( để đẩy code lên repository )
// Hello