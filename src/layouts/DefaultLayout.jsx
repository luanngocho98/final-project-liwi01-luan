import React from 'react'
import { Outlet } from 'react-router-dom'
import Header from '../components/header'

export default function DefaultLayout() {
  return (
    <div>
      <Header />
      <Outlet />
      <h1>FOOTER</h1>
    </div>
  )
}
